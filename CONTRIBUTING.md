Contributing Guide
==================

Bug Reports
-----------

Bug reports can be posted in a gitlab issue.

Pull Requests
-------------

If you would like to add some feature to htim, you can also
start a pull request on gitlab.

Feature Requests
----------------

If you have a feature request or any problem or idea, you can email me
at thelinuxguy9@gmail.com

Support
-------

If you want to support my work, you can by me a coffee.

BTC: 1JbJobcehGT16ajWgYKjTrazdTVHEcTNAx

ETH: 0x2A9a73Ef69a5B7973dC0B32C74cb37BDF33e2a60

monero: 48c54eNSbzTjcSbGeYWyEuLLguBVM6VCiUuVPhmWhFHSPdi6t9rxuVi4AMD6HfhGupgfHfKUy4ZJkdQpbYWZuAcqSLHxFVu

bitcoincash:qz8kh6w8rfr4atn4uvglqwlln6fkcac23ssmddudmp

LTC: LNX3Yi8kwT1Lw6sVkZmPFTh6n4p9umxrTB

DASH: XizbnFcPW58UHAyv5H4WdKX5LoujHyhskK

ETC: 0xAD480bAc3d100242E41920ad861D6e26d0F66f7f

ZSH: t1fSrqskU3o8H262fXrYnLdiFb4HnbkMgoV

DOGE: DR8b2Qu46fm51YarKiCWiMc2VRUWWqQnvg

RVN: RAZNVmNnihcMbc3wALZuNksdaFpGthi7yW

BEAM: 2DWjfstQoaLMYYgvc4NBTTGqRvxWPAWJyNKcHByWxk6aPCnWr8DUhyQVjp4snSAHQqJarDkEAKt2J58zsAx2DmUTcsJjyReYC5FF9yVBHDcRB5ktxE6VoE5nkuX6KA7NA2YHM8mCQUqZJSLQxLjGSsgLbXfmUD26vtGEsg4y6LQg5CiiQr8QKvKhc6wm9iJAuNVjkg4CF6fimcKvQ7v5TiFFY55ZLwwmnqoUAZWzfcgad8JZu7ogEXYNWLALG6f71QX2xN1CVwo5f7b6GxtZ4Qcxf1qz4tdWJ5wMZcUHriUieGaRS2xja2zNQUzhmEeWW2Ww8HsB4pvkEYnirV7VceWPsc9kmRGJEnzez7zncFodQTiEmJ27FaVhxjPEzSKF7jpzLygwXE8YLaA5icDCmWss8L1cL1tr88epQ2CtSsatbvpvSRoBxSJiBugPxV6H8BZiGHPnmfAc6tUixcb8m5po4sE9iTSsMdsz69Srp9tW6zTnqRoxiz99AZJCTxDkpRfyV

Paypal: https://paypal.me/AppDevelop?locale.x=en_US
